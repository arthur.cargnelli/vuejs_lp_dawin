import Vue from 'vue' //librairie "vue" dans node_modules
import app from './app.vue' //fichier app.vue local
import MovieItemComponent from './components/movieitem.vue'
import VueRouter from 'vue-router'
import Vuetify from 'vuetify'
import vSelect from 'vue-select'
import ProgressBar from 'vue-simple-progress'
import 'vuetify/dist/vuetify.min.css'
import router from './routes.js'
import { Slide } from 'vue-burger-menu'
import { ScaleRotate } from 'vue-burger-menu'
import VueFirestore from 'vue-firestore';

 

Vue.use(Vuetify)

Vue.use(VueRouter)

Vue.use(VueFirestore);

Vue.component('Slide',Slide)
Vue.component('ScaleRotate',ScaleRotate)
Vue.component('v-select', vSelect);
Vue.component('v-progress',ProgressBar);
Vue.component('movie-item', MovieItemComponent);

Vue.config.productionTip = false;



new Vue({
  el: '#app2',
  router,
  render: h => h(app)
})