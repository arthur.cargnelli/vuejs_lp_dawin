import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeComponent from './components/home.vue'
import EditMovieComponent from './components/editmovie.vue'
import InfoMovieComponent from './components/movieinfo.vue'
import New from './components/new.vue'
import Layout from './components/layout.vue'
import About from './components/about.vue'
import Author from './components/author.vue'

const layout = Vue.component('layout',Layout);

const about = Vue.component('about',About);

const author = Vue.component('author',Author);

const home = Vue.component('home',HomeComponent)

const editmovie = Vue.component('editmovie',EditMovieComponent)

const movieinfo = Vue.component('movieinfo',InfoMovieComponent)

const newmovie = Vue.component('newmovie',New)


const routes = [{
    path:"/",
    name:"layout",
    component:layout,
    children:[
        {
            path:'',
            name:'home',
            component: home
        },

        {
            path:'/about',
            name:'about',
            component: about
        },

        {
            path:'/author',
            name:'author',
            component: author
        },

        {
            path:'/addnew',
            name:'newmovie',
            component: newmovie
        },

        {
            path:'/movie/:movie',
            name:'movie',
            component: movieinfo
        },
        
        { path: '/movie/:movie/edit',
        name:'edit',
        component:  editmovie,
        }
    ]
}]


export default new VueRouter({
    routes
  })  