import firebase from 'firebase/app'
import 'firebase/firestore'

const firebaseConfig = {
    apiKey: "AIzaSyAmeZyt97ukQt9JsAQmH3pAU2qjV4sQw44",
    authDomain: "projetvuejs.firebaseapp.com",
    databaseURL: "https://projetvuejs.firebaseio.com",
    projectId: "projetvuejs",
    storageBucket: "projetvuejs.appspot.com",
    messagingSenderId: "187828896422",
    appId: "1:187828896422:web:2e86b8b264ef9b30566a67",
    measurementId: "G-95ECYZNECN"
};
// Initialize Firebase
const firebaseApp = firebase.initializeApp(firebaseConfig);
// firebase.analytics();

export const db = firebaseApp.firestore();