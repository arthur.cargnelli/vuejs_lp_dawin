FROM node:latest

COPY ./VueJS/ /VueJs
COPY ./VueJS/package.json /VueJs/package.json

WORKDIR /VueJs

RUN npm i ajv --save 
RUN npm i firebase --save 
RUN npm i

ENV PORT=9000

CMD npm start
